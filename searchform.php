    
    <form role="search" method="get" class="form-inline form-horizontal search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input type="search" class="form-control" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'orange' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
        <button type="submit" class="search-submit btn btn-info" value="<?php echo _e( '搜索', 'orange' ); ?>"><?php echo _e( '搜索', 'orange' ); ?></button>
    </form>
<?php 
    
/**
 * Adds Cooperation_Widget widget.
 */
class Cooperation_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'Cooperation_widget', // Base ID
            __( '合作伙伴', 'orange' ), // Name
            array( 'description' => __( '合作伙伴', 'orange' ), ) // Args
            );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['image_uri'] ) ) {
            echo "<img class='center-block img-responsive' src='". esc_url( $instance['image_uri'], null, 'display' ) ."'>" ;
        }
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '新标题', 'orange' );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('image_uri'); ?>"><?php _e('图片', 'orange'); ?></label><br/>
            <?php
            if ( !empty($instance['image_uri']) ) :

                echo '<img class="custom_media_image_testimonial" src="' . $instance['image_uri'] . '" style="margin:0;padding:0;max-width:100px;float:left;display:inline-block" alt="'.__( '上传图片', 'orange' ).'" /><br />';

            endif;

            ?>
            <input type="text" class="widefat custom_media_url_testimonial" name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php if( !empty($instance['image_uri']) ): echo $instance['image_uri']; endif; ?>" style="margin-top:5px;">
            <input type="button" class="button button-primary custom_media_button_testimonial" id="custom_media_button_testimonial" name="<?php echo $this->get_field_name('image_uri'); ?>" value="<?php _e('上传图片','orange'); ?>" style="margin-top:5px;">
        </p>
        
        <input class="custom_media_id" id="<?php echo $this->get_field_id( 'custom_media_id' ); ?>" name="<?php echo $this->get_field_name( 'custom_media_id' ); ?>" type="hidden" value="<?php if( !empty($instance["custom_media_id"]) ): echo $instance["custom_media_id"]; endif; ?>" />

        <?php 
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['custom_media_id'] = strip_tags($new_instance['custom_media_id']);

        return $instance;
    }

} // class Cooperation_Widget

// Register custom widgets
add_action( 'widgets_init', function(){
    register_widget( 'Cooperation_Widget' );
});

add_action('admin_enqueue_scripts', 'cooperation_widget_scripts');
function cooperation_widget_scripts() {
    wp_enqueue_media();
    wp_enqueue_script('cooperation_widget_script', get_template_directory_uri() . '/scripts/widget-cooperation.js', false, ORANGE_VERSION, true);
}
<?php 
    
    class Orange_Customizer {
        public static function register($wp_customize) {
         
           /* $wp_customize->add_section( 
                'cooperation', 
                array(
                    'title'       => __( '合作伙伴', 'orange' ),
                    'priority'    => 1001,
                    'capability'  => 'edit_theme_options',
                    'description' => __('Change footer bg-color options here.', 'orange'), 
                ) 
            );*/

            // platform bg color
            $wp_customize->add_setting( 'platform_bg_color',
                array(
                    'default' => 'ffffff',
                    'transport' => 'postMessage'
                )
            );
            $wp_customize->add_control( new WP_Customize_Color_Control(
                $wp_customize,
                'platform_bg_color',
                array(
                    'label'    => __( '平台区颜色', 'orange' ), 
                    'section'  => 'colors',
                    'settings' => 'platform_bg_color',
                    'priority' => 12,
                ) 
            ) );

            // footer bg color
            $wp_customize->add_setting( 'footer_bg_color',
                array(
                    'default' => 'ffffff',
                    'transport' => 'postMessage'
                )
            );
            $wp_customize->add_control( new WP_Customize_Color_Control(
                $wp_customize, 
                'footer_bg_color_control',
                array(
                    'label'    => __( '底部区颜色', 'orange' ), 
                    'section'  => 'colors',
                    'settings' => 'footer_bg_color',
                    'priority' => 11,
                ) 
            ));
            // cooperation bg color
            $wp_customize->add_setting( 'cooperation_bg_color',
                array(
                    'default' => 'ffffff',
                    'transport' => 'postMessage'
                )
            );
            $wp_customize->add_control( new WP_Customize_Color_Control(
                $wp_customize,
                'cooperation_bg_color_control',
                array(
                    'label'    => __( '合作区颜色', 'orange' ), 
                    'section'  => 'colors',
                    'settings' => 'cooperation_bg_color',
                    'priority' => 10,
                ) 
            ) );
            // qualify bg color
            $wp_customize->add_setting( 'qualify_bg_color',
                array(
                    'default' => 'ffffff',
                    'transport' => 'postMessage'
                )
            );
            $wp_customize->add_control( new WP_Customize_Color_Control(
                $wp_customize,
                'qualify_bg_color_control',
                array(
                    'label'    => __( '资质区颜色', 'orange' ), 
                    'section'  => 'colors',
                    'settings' => 'qualify_bg_color',
                    'priority' => 13,
                ) 
            ) );
        }
        public static function header_output() {
            ?>
                <style type='text/css'>
                    footer {
                        background-color:<?php echo get_theme_mod('footer_bg_color'); ?> ;
                    }
                    .platform{
                        background-color:<?php echo get_theme_mod('platform_bg_color'); ?> ;
                    }
                    .qualify{
                        background-color:<?php echo get_theme_mod('qualify_bg_color'); ?> ;
                    }
                    .cooperation{
                        background-color:<?php echo get_theme_mod('cooperation_bg_color'); ?> ;
                    }
                </style>
            <?php
        }
        public static function live_preview() {
            wp_enqueue_script('orange_theme_customizer',get_template_directory_uri().'/scripts/theme-customizer.js', ['jquery', 'customize-preview'],'1.0',true);
        }
    }

    add_action( 'customize_register' , array( 'Orange_Customizer' , 'register' ) );
    add_action( 'wp_head' , array( 'Orange_Customizer' , 'header_output' ) );
    add_action( 'customize_preview_init' , array( 'Orange_Customizer' , 'live_preview' ) );


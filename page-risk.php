<?php
/*
Template Name: risk-control
*/
?>

<?php 
    get_header();
?>

    <div class="risk-control" id="risk-control">
        <div class="container">
            <p class="product-title text-center"><?php _e( '风险管控', 'loquat' ); ?></p>
            <div class="row text-center">
                <div class="col-md-4">
                    <img class="center-block risk-control-img" src="<?php echo get_template_directory_uri() ?>/images/贷前调查.png" alt="<?php esc_attr_e( '贷前调查', 'loquat' ); ?>">
                    <dl>
                        <dt class="risk-control-title"><?php _e( '贷前调查', 'loquat' ); ?></dt>
                        <dd><?php _e( '根据企业经营数据和第三方认证数据，辨析企业经营状况，实时为企业画像', 'loquat' ); ?></dd>
                    </dl>
                </div>
                <div class="col-md-4">
                    <img class="center-block risk-control-img" src="<?php echo get_template_directory_uri() ?>/images/贷中审核.png" alt="<?php esc_attr_e( '贷中审核', 'loquat' ); ?>">
                    <dl>
                        <dt class="risk-control-title"><?php _e( '贷中审核', 'loquat' ); ?></dt>
                        <dd><?php _e( '通过数据分析平台实时监控企业的交易状况和现金流，为风险预警提供信息输入', 'loquat' ); ?></dd>
                    </dl>
                </div>
                <div class="col-md-4">
                    <img class="center-block risk-control-img" src="<?php echo get_template_directory_uri() ?>/images/贷后监管.png" alt="<?php esc_attr_e( '贷后监管', 'loquat' ); ?>">
                    <dl>
                        <dt class="risk-control-title"><?php _e( '贷后监管', 'loquat' ); ?></dt>
                        <dd><?php _e( '通过实时监控企业的经营和行为，得到可能影响正常履约的动态预警信息', 'loquat' ); ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    
    <style>
        .risk-control{
            background: #00050d;
        }
        .risk-control dd{
            color: white;
            width: 241px;
            margin: 0 auto;
            background: url(<?php echo get_template_directory_uri(); ?>/images/line3.png) no-repeat center center;
            height: 70px;
            font-size: 14px;
            padding-left: 20px;
            padding-right: 20px;
            padding-top: 5px;
        }
        .risk-control-title{
            color: #35acff;
            font-size: 2rem;
            margin-bottom: 2rem;
        }
        .risk-control-img{
            margin-bottom: 2rem;
        }
    </style>

    <script>
        jQuery(function ( $ ) {
            $('#risk-control').css('minHeight', $(window).height() - $('#header').height() );
        });
    </script>

<?php 
    get_footer( 'none' );
?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <meta name="renderer" content="webkit">
    <?php seo(); wp_head(); ?>
</head>
<body <?php body_class('zengxiaoluan');?>>

    <header id="header" class="hidden-print header">
        <nav id="nav" >
            <div class="logo pull-left" id="logo">
                <?php 
                    if ( has_custom_logo() ) :
                        the_custom_logo();
                    else:
                ?>
                <a href="<?php echo esc_url( home_url('home') ); ?>">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>">
                </a>
                <?php endif; ?>
                <div id="menu-toggle" class="menu-toggle pull-right">
                    <span class="line1"></span>
                    <span class="line2"></span>
                    <span class="line3"></span>
                </div>
            </div>
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_class'     => 'primary-menu',
                    'menu_id'        => 'orange-menu',
                    'container'      => 'div',
                    'container_class'=> 'menu',
                    'fallback_cb'    => false,
                    'container_id'   => 'menu',
                    'echo'           => true,
                ) );
            ?>
        </nav>
    </header>

    <style>
        .header{
            background: black;
        }
        .header .menu-item a{
            color: #eee;
            text-decoration: none;
            display: inline-block;
        }
        .header .menu-item:hover{
            border-bottom: 3px solid #35acff;
        }
        .header .current-menu-item{
            border-bottom: 3px solid #35acff;
        }
        .menu-toggle{
            cursor: pointer;
            width: 36px;
            height: 24px;
            position: relative;
        }
        .menu-toggle.open .line1{
            transform: rotate(-45deg);
        }
        .menu-toggle.open .line2{
            opacity: 0;
        }
        .menu-toggle.open .line3{
            transform: rotate(45deg);
        }
        .menu-toggle span{
            position: absolute;
            width: 29px;
            height: 3px;
            background-color: white;
            display: inline-block;

            border-radius: 2px;
            
            transition: all .5s;
            transform-origin: 28px 2px;
        }
        .menu-toggle span.line2{
            top: 10px;
        }
        .menu-toggle span.line3{
            top: 20px;
        }
        .breadcrumbs span[property*='itemListElement']:last-child{
            max-width: 375px;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap; 
        }
        #header .logo img {
            margin-left: 24px;
            margin-left: 2em;
        }
    </style>
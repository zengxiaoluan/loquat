<?php 
    get_header();    
?>

    <?php
        while ( have_posts() ) : the_post();
    ?>
    
        <div class="container">
            <div class="row">
                <div <?php post_class('col-md-8 '); ?> id="post-<?php the_ID(); ?>" >
                    <div class="article">
                        <h2><?php the_title(); ?></h2>
                        <div class="h5 help-block">
                            <span title="<?php _e( '文章作者', 'orange' ); ?>">
                                <?php echo get_avatar( get_the_author_meta( 'user_email' ), 42, '', true ); ?>
                                <?php the_author_posts_link(); ?>
                                <?php //the_author_link(); ?>
                            </span>
                            <span title="<?php _e( '发布时间', 'orange' ); ?>">
                                <i class="fa fa-clock-o fa-lg" aria-hidden="true" title="<?php _e( '发布时间', 'orange' ); ?>"></i>
                                <abbr><?php the_date('Y-m-d g:i:s a'); ?></abbr>
                            </span>
                            <span title="<?php _e( '评论次数', 'orange' ); ?>">
                                <?php 
                                    // echo get_comments_number();
                                    comments_popup_link( '<i class="fa fa-comments fa-lg" aria-hidden="true"></i>', 
                                        '<i class="fa fa-comments fa-lg" aria-hidden="true"></i> 1 ', '<i class="fa fa-comments fa-lg" aria-hidden="true"></i> % ', '', false );
                                ?>
                            </span>
                            <span title="<?php _e( '阅读次数', 'orange' ); ?>">
                                <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                <abbr><?php echo get_post_meta( get_the_ID(), 'views', true );?></abbr>
                            </span>
                            <span title="<?php _e( '相关标签', 'orange' ); ?>">
                                <i class="fa fa-tags fa-lg" aria-hidden="true"></i>
                                <abbr><?php the_tags( ' ', ' | ', ' ' ); ?></abbr>
                            </span>
                            <span title="<?php _e( '文章归类', 'orange' ); ?>">
                                <i class="fa fa-folder fa-lg" aria-hidden="true"></i>
                                <?php 
                                    the_category( '|', '', false );
                                 ?>
                            </span>
                        </div>
                        <?php the_post_thumbnail('full',['class'=>'img-responsive']); ?>
                        <?php the_content(); ?>

                        <!-- 百度分享按钮 -->
                    <!--     
                    <div class="bdsharebuttonbox">
                        <a href="#" class="bds_more" data-cmd="more"></a>
                        <a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
                        <a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
                        <a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a>
                        <a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a>
                        <a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
                    </div>
                    <script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"1","bdSize":"32"},"share":{},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"32"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='<?php echo get_template_directory_uri(); ?>/scripts/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
                    </script>
                     -->
                        <?php the_post_navigation(array(
                                    'prev_text'           => '<i class="fa fa-arrow-left" aria-hidden="true"></i><span>%title</span>',
                                    'next_text'           => '<span>%title</span><i class="fa fa-arrow-right" aria-hidden="true"></i>',
                                    'in_same_term'        => false,
                                ));
                        ?>
                        <?php wp_link_pages( ); ?>
                    </div>

                    <?php comments_template('',true); ?>
                </div>

                <?php get_sidebar(); ?>

            </div>
        </div>

    <?php 
        endwhile;
    ?>

<?php 
    get_footer();
?>

<style>
    .post .h5{
        padding-bottom: 1em;
        border-bottom: 1px dotted #eee;
    }
    .container{
        margin-top: 1em;
    }
    .post #sidebar li{
        list-style: none;
    }
    .post .article abbr{
        font-size: 12px;
    }
    .post:hover{
        box-shadow: 0px 1px 15px #d2d0d0,0px -1px 15px #d2d0d0;
    }
    .post .article > p{
        margin-top: 2em;
        line-height: 1.5em;
    }
    .post img{
        height: auto;
        border: 1px dotted #eee;
        max-width: 100%;
    }
</style>
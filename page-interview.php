<?php
/*
Template Name: interview
*/
?>

<?php 
    get_header();
 ?>
    
    <div class="container">
        
        <?php
            while ( have_posts() ) : the_post();
        ?>
                <?php the_content(); ?>

        <?php 
            endwhile;
        ?>

        <div id="modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <h1><?php _e( '提示', 'loquat' ); ?></h1>
                    <p><?php _e( '你已经处于离线状态，30秒后将自动提交！', 'loquat' ); ?></p>
                </div>
            </div>
        </div>

    </div>

    <script>
        jQuery(document).ready(function($) {
            var off_line = false

            $(window).blur(function(event) {
                $('#modal').modal()
                off_line = true
                setTimeout(function () {
                    if ( off_line ) {
                        console.log('submit')
                        $('form.wpcf7-form').trigger('submit')
                    }
                }, 30000)
            }).focus(function(event) {
                off_line = false
            });

            setInterval( function(){
                if( document.hidden ){
                    alert('<?php _e( "请勿离开本页面", "loquat" ) ?>');
                }
            }, 5000 );

        });
    </script>

<?php get_footer();?>
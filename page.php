<?php 
    get_header();    
?>

    <?php
        while ( have_posts() ) : the_post();
    ?>

        <div class="container">
            <h1><?php the_title(); ?></h1>
            <span><?php the_views(); ?></span>
            <?php the_content(); ?>
            <?php
            
                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . __( '所有页面：', 'orange' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '<span class="screen-reader-text">' . __( '页面', 'orange' ) . ' </span>%',
                    'separator'   => '<span class="screen-reader-text">, </span>',
                ) );

                comments_template();

             ?>
        </div>

    <?php 
        endwhile;
    ?>

<?php 
    get_footer();
?>
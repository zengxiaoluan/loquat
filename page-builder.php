<?php
/*
Template Name: builder
*/
?>

<?php 
    get_header();
?>

    <div class="retailer">
        <div class="container">
            <p class="product-title text-center"><?php _e( '工程融资贷', 'loquat' ); ?></p>
            <?php if( wp_is_mobile() ): ?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/4.png" alt="<?php esc_attr_e( bloginfo( 'name' ), 'loquat' ); ?>" class="img-responsive">
            <?php else: ?>
                <video autoplay loop class="center-block" poster="<?php echo get_template_directory_uri(); ?>/images/4.png">
                    <source src="<?php echo get_template_directory_uri(); ?>/video/4.mp4">
                </video>
            <?php endif; ?>
            <p class="product-desc text-center"><?php _e( '为建筑工程项目提供全流程的融资服务', 'loquat' ); ?></p>
            <div class="features row text-center">
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '额度高', 'loquat' ); ?></p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '申请快', 'loquat' ); ?></p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '渠道多', 'loquat' ); ?></p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '可控性', 'loquat' ); ?></p>
                </div>
            </div>
            <div class="retailer-bank text-center">
                <p><a href="https://welinkcredit.com/loan-application">&#60;&#60;<?php _e( '申请贷款', 'loquat' ); ?></a></p>
            </div>

        </div>
    </div>

<?php 
    get_footer( 'none' );
?>

<style>
    .features{
        margin-bottom: 2rem;
    }
    .product-desc{
        margin-bottom: 4rem;
    }
    .retailer-feature{
        color: #35acff;
        padding: 1rem 2rem;
        border: 1px solid #35acff;
        border-radius: 3px;
        margin: 0 4rem 1rem 4rem;
    }
    .retailer{
        padding-bottom: 8rem;
    }
</style>
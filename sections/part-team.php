    
    <!-- team -->
    <div class="row teams">
        <?php 
            // $the_query = new WP_Query(['category_name'=>'team']);
            // while ($the_query->have_posts()) : $the_query->the_post();
        ?>
        <h2 class="text-center text-muted"><strong><?php _e( '核心成员', 'orange' ); ?></strong></h2>
        <div class="col-md-3">
            <div class="team text-center">
                <img class="img-responsive img-circle" src="<?php echo get_template_directory_uri(); ?>/images/褚楠.png" title="褚楠，CEO" alt="褚楠，CEO">
                <h3><?php _e( '褚楠', 'orange' ); ?></h3>
                <h5>CEO</h5>
                <div class="border"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="team text-center">
                <img class="img-responsive img-circle" src="<?php echo get_template_directory_uri(); ?>/images/卜俊.png" title="卜俊，Data Mining Supervisor" alt="卜俊，Data Mining Supervisor">
                <h3><?php _e( '卜俊', 'orange' ); ?></h3>
                <h5>Data Mining Supervisor</h5>
                <div class="border"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="team text-center">
                <img class="img-responsive img-circle" src="<?php echo get_template_directory_uri(); ?>/images/姚威.png" title="姚威，Information Security Leader" alt="姚威，Information Security Leader">
                <h3><?php _e( '姚威', 'orange' ); ?></h3>
                <h5>Information Security Leader</h5>
                <div class="border"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="team text-center">
                <img class="img-responsive img-circle" src="<?php echo get_template_directory_uri(); ?>/images/黄威杰.png" title="黄威杰，Database Director" alt="黄威杰，Database Director">
                <h3><?php _e( '黄威杰', 'orange' ); ?></h3>
                <h5>Database Director</h5>
                <div class="border"></div>
            </div>
        </div>
        <?php 
        // endwhile;
        ?>
    </div>

    <style>
        .teams{
            margin-top: 4em;
        }
    </style>

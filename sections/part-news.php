
        <div class="post">
            <div class="magic">
                <div class="feature-img">
                    <?php the_post_thumbnail('medium_large',['class'=>'img-responsive']); ?>
                </div>
                <div class="meta">
                    <h4>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h4>
                    <div class="h5">
                        <span title="<?php _e( '发布时间', 'orange' ); ?>" class="text-success">
                            <i class="fa fa-clock-o fa-lg" aria-hidden="true" title="<?php _e( '发布时间', 'orange' ); ?>"></i>
                            <abbr><?php printf('%s', get_the_date("Y-m-d H:i:s")); ?></abbr>
                        </span>
                    </div>
                    <div class="excerpt">
                        <h5 class="text-muted"><?php the_excerpt(); ?></h5>
                    </div>
                </div>
            </div>
        </div>
        

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header h4">
            <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>
        </header>

        <?php the_excerpt(); ?>

        <?php if ( 'post' === get_post_type() ) : ?>

            <?php
                edit_post_link(
                    sprintf(
                        /* translators: %s: Name of current post */
                        __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'orange' ),
                        get_the_title()
                    ),
                    '<span class="edit-link">',
                    '</span>'
                );
            ?>

        <?php endif; ?>
    </article>


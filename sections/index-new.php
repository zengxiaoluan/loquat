    
    <div class="section news container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title text-center text-muted"><?php _e( '文沥名家', 'orange' ); ?></div>
                </div>
            </div>
            <div class="row">
                <div id="carousel-news-generic" class="carousel slide" data-ride="carousel" data-interval="10000" data-pause="hover">
                    <div class="carousel-inner" role="listbox">
                        <?php 
                        
                        $the_query = new WP_Query(array( 'post_type' => 'post','posts_per_page' => 20 ));

                        $newSlideActive = '<div class="item active">';
                        $newSlide       = '<div class="item">';
                        $close  = '</div>';
                        $i_latest_posts = 0;

                        while ($the_query->have_posts()) : $the_query->the_post();

                        $i_latest_posts++;

                        if ($i_latest_posts == 1) {
                            echo $newSlideActive;
                        }elseif ($i_latest_posts % 10 == 1) {
                            echo $newSlide;
                        }
                        
                        echo "<div class='col-md-10'>";
                        echo "<div class='col-md-12 article'>";
                        ?>
                        <span class="h4"><a target="_blank" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
                        <span><?php the_author(); ?></span>
                        <time class='h5'><?php printf('%s',get_the_date()); ?></time>
                        <?php
                        echo $close;
                        echo $close;
                        
                        if ($i_latest_posts % 10 == 0 ) {
                            echo $close;
                        }

                        endwhile;

                        if ($i_latest_posts % 10 != 0) {
                            echo $close;
                        }
                        ?>
                    </div>
                    
                    <a class="left carousel-control" href="#carousel-news-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only"><?php _e( '上一个', 'orange' ); ?></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-news-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only"><?php _e( '下一个', 'orange' ); ?>Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
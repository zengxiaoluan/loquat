    
    <div class="section qualify">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-line">
                        <div class="title text-muted text-center"><?php _e( '公司资质', 'orange' ); ?></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/电商数据贷软件著作权.jpg" alt="电商数据贷软件著作权">
                </div>
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/工程融资软件著作权.jpg" alt="电商数据贷软件著作权">
                </div>
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/文沥供应链大数据分析软件.jpg" alt="电商数据贷软件著作权">
                </div>
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/文沥供应链金融风险管理软件.jpg" alt="电商数据贷软件著作权">
                </div>
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/文沥供应链物流管理软件.jpg" alt="电商数据贷软件著作权">
                </div>
                <div class="col-md-2 col-sm-6 col-xs-6">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/文沥征信系统安全等级报告.jpg" alt="电商数据贷软件著作权">
                </div>
            </div>
        </div>
    </div>
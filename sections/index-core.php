    
    <div class="section core">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12">
                    <div class="title"><?php _e( '风险管控', 'orange' ); ?></div>
                </div>
            </div>
            <div class="col-md-4 loan">
                <figure>
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/贷前.jpg" alt=""> -->
                    <i class="fa fa-cogs fa-5x" aria-hidden="true"></i>
                    <figcaption class="text-muted">
                        <h1><?php _e( '贷前调查', 'orange' ); ?></h1>
                        <p><?php _e( '根据企业经营数据和第三方认证数据，辨析企业经营状况，实时为企业画像。', 'orange' ); ?></p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-4 loan">
                <figure>
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/贷中.jpg" alt=""> -->
                    <i class="fa fa-check-circle-o fa-5x" aria-hidden="true"></i>
                    <figcaption class="text-muted">
                        <h1><?php _e( '贷中审核', 'orange' ); ?></h1>
                        <p><?php _e( '通过数据分析平台实时监控企业的交易状况和现金流，为风险预警提供信息输入。', 'orange' ); ?></p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-4 loan">
                <figure>
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/贷后.jpg" alt=""> -->
                    <i class="fa fa-heartbeat fa-5x" aria-hidden="true"></i>
                    <figcaption class="text-muted">
                        <h1><?php _e( '贷后监管', 'orange' ); ?></h1>
                        <p><?php _e( '通过监控企业经营动态和行为，可能影响正常履约的行为将被预警，建立贷后监控系统。', 'orange' ); ?></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <style>
        .core{
            background-color: #eee;
        }
        .loan{
            padding: 20px;
        }
        .loan:hover{
            background-color: #0e8ece;
            color: white;
        }
        .loan:hover h1,.loan:hover p{
            color: white;
        }
    </style>
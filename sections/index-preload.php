    
    <div id="preload" class="preload">
        <?php if ( wp_is_mobile() ):?>
            <img class="preload" src="<?php echo get_template_directory_uri(); ?>/images/1.png" alt="<?php esc_attr_e( bloginfo( 'name' ), 'loquat' ); ?>">
        <?php else: ?>
            <video autoplay preload loop poster="<?php echo get_template_directory_uri(); ?>/images/1.png">
                <img src="<?php echo get_template_directory_uri(); ?>/images/1.png" alt="">
                <source src="<?php echo get_template_directory_uri(); ?>/video/1.mp4">
            </video>
        <?php endif; ?>
        <div id="text" class="text">
            <p class="text-head">数据为本  科技铸金</p>
            <p class="text-desc">做全球领先的金融科技服务商</p>
            <a class="enter" id="enter" href="<?php echo esc_url( home_url( 'home' ) ); ?>">进入官网</a>
        </div>
    </div>    

    <style>
        .preload{
            position: absolute;
            top: 0px;
            right: 0px;
            bottom: 0px;
            left: 0px;

            overflow: hidden;

            display: flex;
            justify-content: center;
            align-items: center;
        }
        .preload video{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;

            -webkit-object-fit: cover;
            -ms-object-fit: cover;
            -o-object-fit: cover;
            object-fit: cover;
            z-index: 1;
        }
        .is-ie{
            position: absolute;
            top: 50%;
            left: 50%;
            width: 800px;
            height: 400px;
            margin-left: -400px;
            margin-top: -200px;
        }
        .is-ie .enter{

        }
        .preload .text{
            font-family: '方正兰亭超细黑简体';
            color: white;
            text-align: center;

            z-index: 2;
        }
        .text-head{
            font-size: 84px;
            font-size: 7rem;
            margin-bottom: 1.5rem;
            text-shadow: 0px 1px 4px #0082ff,1px 0px 4px #0082ff; 
        }
        .text-desc{
            font-size: 60px;
            font-size: 5rem;
            margin-bottom: 1.5rem;
            text-shadow: 0px 1px 4px #0082ff,1px 0px 4px #0082ff; 
        }
        .enter{
            display: inline-block;
            border: 1px solid white;
            background: transparent;
            color: white;
            padding: 12px 24px;
            padding: 1rem 2rem;
            font-size: 18px;
            font-size: 1.5rem;
            outline: none;
            border-radius: 5px;
            text-decoration: none;

            transition: all 0.5s;
        }
        .enter:hover{
            background: white;
            color: #011128;
            border: 1px solid #011128;
            text-decoration: none;
        }
        .enter:active, .enter:focus{
            text-decoration: none;
        }
        @media screen and (max-width: 32.1em) {
            .text-head{
                font-size: 3rem;
            }
            .text-desc{
                font-size: 2rem;
            }
        }
    </style>

    <script>
        (function ( $ ) {
            if( $.browser.msie && $.browser.version < 10 ){
                $('video').remove();
                $('#preload').append('<img class="preload" src="<?php echo get_template_directory_uri(); ?>/images/1.png" alt="<?php esc_attr_e( bloginfo( 'name' ), 'loquat' ); ?>"')
                console.log( $.browser.version )
                $('#text').addClass('is-ie');
            }
        })( jQuery );
    </script>
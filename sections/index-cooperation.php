        
        <div class="section cooperation">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title text-center text-muted"><?php _e( '合作伙伴', 'orange' ); ?></div>
                    </div>
                </div>
                <?php // the_widget( 'Cooperation_Widget' ); ?>
                
                <div class="row text-center">
                    <?php if ( is_active_sidebar( 'cooperation' ) ): ?>

                        <?php dynamic_sidebar( 'cooperation' ); ?>

                    <?php else: ?>
                            
                        <div class="col-md-2 col-sm-4 col-xs-12">
                            <img class="center-block" src="<?php echo get_template_directory_uri(); ?>/images/长沙.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-12">
                            <img class="center-block" src="<?php echo get_template_directory_uri(); ?>/images/平安.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-12">
                            <img class="center-block" src="<?php echo get_template_directory_uri(); ?>/images/华夏.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-12">
                            <img class="center-block" src="<?php echo get_template_directory_uri(); ?>/images/光大.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-12">
                            <img class="center-block" src="<?php echo get_template_directory_uri(); ?>/images/中南.png" alt="" class="img-responsive">
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-12">
                            <img class="center-block" src="<?php echo get_template_directory_uri(); ?>/images/金螳螂.jpg" alt="" class="img-responsive">
                        </div>
                    
                    <?php endif; ?>

                </div>
            </div>
        </div>

        <style>
            .cooperation img{
                margin-bottom: 10px;
                filter: grayscale(100%);
            }
            .cooperation img:hover{
                filter: grayscale(0%);
            }
        </style>

    <div class="section platform">
        <div class="container">
            <ul id="scene">
                <li class="layer" data-depth="0.00"><img src="<?php echo get_template_directory_uri(); ?>/images/pf1.png"></li>
                <li class="layer" style="z-index: -1;" data-depth="0.20"><img src="<?php echo get_template_directory_uri(); ?>/images/pf2.png"></li>
                <li class="layer" data-depth="0.40"><img src="<?php echo get_template_directory_uri(); ?>/images/pf3.png"></li>
                <li class="layer" data-depth="0.60"><img src="<?php echo get_template_directory_uri(); ?>/images/pf4.png"></li>
            </ul>
        </div>
    </div>

    <script>

        jQuery(document).ready(function($) {
            (function($){
                $('#scene').parallax();
            })(jQuery); 
        });

    </script>
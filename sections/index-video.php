    
    <div class="service" id="service">
        <div class="video">
            <video autoplay loop>
                <source src="<?php echo get_template_directory_uri(); ?>/video/2.mp4">
            </video>
        </div>

        <div class="container">
            <div class="row first-line">
                <div class="col-md-4">
                    <span class="service-title h4"><?php _e( '企业画像', 'loquat' ); ?></span>
                    <span class="service-line"></span>
                    <div class="service-desc">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon_1.png" alt="<?php esc_attr_e( '企业画像', 'loquat' ); ?>">
                        <span class="fadeIn animated-slow">
                            <?php _e( '实时采集中小企业各类经营数据，<br>及时判断企业运营情况', 'loquat' ); ?>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <span class="service-title h4"><?php _e( '企业征信', 'loquat' ); ?></span>
                    <span class="service-line"></span>
                    <div class="service-desc">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon_2.png" alt="<?php esc_attr_e( '企业画像', 'loquat' ); ?>">
                        <span class="fadeIn animated-slow">
                            <?php _e( '根据实时的企业画像，为各类机构<br>提供不同行业的精准动态信息', 'loquat' ); ?>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <span class="service-title h4"><?php _e( '金融科技', 'loquat' ); ?></span>
                    <span class="service-line"></span>
                    <div class="service-desc">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon_3.png" alt="<?php esc_attr_e( '企业画像', 'loquat' ); ?>">
                        <span class="fadeIn animated-slow">
                            <?php _e( '以科技的力量为金融机构拓展服务<br>范围与加快审核效率，促进金融行<br>业的创新', 'loquat' ); ?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="cleafix"></div>
            <div class="row">
                <div class="col-md-4">
                    <span class="service-title h4"><?php _e( '大数据', 'loquat' ); ?></span>
                    <span class="service-line"></span>
                    <div class="service-desc">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon_4.png" alt="<?php esc_attr_e( '企业画像', 'loquat' ); ?>">
                        <span class="fadeIn animated-slow">
                            <?php _e( '深度挖掘、实时处理和海量的积累<br>助于我们在这个时代立于主动位置', 'loquat' ); ?>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <span class="service-title h4"><?php _e( '机器学习', 'loquat' ); ?></span>
                    <span class="service-line"></span>
                    <div class="service-desc">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon_5.png" alt="<?php esc_attr_e( '企业画像', 'loquat' ); ?>">
                        <span class="fadeIn animated-slow">
                            <?php _e( '通过机器学习完善湖南文沥的大数<br>据风控系统，趋近智能化、全局视<br>角化', 'loquat' ); ?>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <span class="service-title h4"><?php _e( '精准营销', 'loquat' ); ?></span>
                    <span class="service-line"></span>
                    <div class="service-desc">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icon_6.png" alt="<?php esc_attr_e( '企业画像', 'loquat' ); ?>">
                        <span class="fadeIn animated-slow">
                            <?php _e( '快速分析客户需求，精准把握行业<br>动态，降低推广成本，实现企业画<br>像价值', 'loquat' ); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .animated-slow{
            -webkit-animation-duration: 5s;
            animation-duration: 5s;
        }
        .service{
            background: #00050d;
            padding-bottom: 3rem;
            display: none;
        }
        .first-line{
            margin-bottom: 3rem;
        }
        .video{
            width: 100%;
        }
        .service video{
            width: 700px;
            height: 700px;

            margin: 0 auto;
            display: block;
            clear: both;
        }
        .service-title{
            display: inline-block;
            width: 100%;
            color: #35acff;
            font-weight: 700;
        }
        .service-line{
            display: inline-block;
            background: url(<?php echo get_template_directory_uri(); ?>/images/line1.gif) no-repeat center center;
            width: 289px;
            height: 11px;
            margin-bottom: 1rem;

            position: relative;
        }
        .service-line:before{
            position: absolute;
        }
        .service-desc{
            color: white;
            height: 62px;
        }
        .service-desc img{
            float: left;
            border: 1px solid #35acff;
        }
        .service-desc span{
            float: left;
            margin-left: 1rem;
        }
    </style>

    <script>
        jQuery(function ( $ ) {
           $('#footer') .css('display', 'none');
           $('#header-top').css('display', 'none');
        });
    </script>
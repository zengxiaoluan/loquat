jQuery(document).ready(function($) {
    /*将整个form设为行内表单*/
    $('#setupform').addClass('form-inline');

    /*使用bootstrap的UI*/
    $('#user_name').addClass( 'form-control' );
    $('#user_email').addClass( 'form-control' );
    $('#si_captcha_code').addClass( 'form-control' );

    /*隐藏默认的WordPress标题*/
    $('#signup-content h2').text('');

    /*提交按钮样式*/
    $('[type*=submit]').addClass('btn btn-success');
});
'use strict';

(function ($) {
    
    $(document).ready(function() {
        var canvas     = document.getElementById('canvas');
        var context    = canvas.getContext('2d');

        canvas.width  = 800;
        canvas.height = 600;

        var image = new Image();
        var clippingRegion = {
            x:200,
            y:400,
            r:50
        }
        image.src = '//' + window.location.hostname + '/wp-content/themes/orange/images/sexy.png';
        image.onload = function(){
            initCanvas();
        }

        function setClippingRegion(clippingRegion){
            context.beginPath();
            context.arc(clippingRegion.x, clippingRegion.y, clippingRegion.r, 0, Math.PI * 2, false);
            context.clip();
        }

        function initCanvas(){
            draw(clippingRegion);
        }

        function draw(clippingRegion){
            context.clearRect(0,0,canvas.width,canvas.height);
            context.save();

            setClippingRegion(clippingRegion);

            context.drawImage(image,0,0);
            context.restore();
        }

        window.reset = function reset(){
            var clippingRegion = {
                r: 50,
                x: Math.random() * 700 + 50,
                y: Math.random() * 500 + 50
            }
            draw(clippingRegion);
        }

        window.show =  function show(){
            var clippingRegion = {
                r: 50,
                x: 0,
                y: 0
            }
            draw(clippingRegion);
            
            var radius = 50;
            var timer = setInterval(function(){
                var clippingRegion = {
                    r: radius + 50,
                    x: 400,
                    y: 300
                }
                console.log('d')
                if (radius <= 400) {
                    radius = radius + 5
                    draw(clippingRegion)
                }else{
                    clearInterval(timer)
                }

            },30)
        }


    });

})(jQuery);
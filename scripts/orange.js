'use strict';

(function($){

    $(document).ready(function() {

        /*导航子菜单hover显示与隐藏*/
        $('.menu-item-has-children').hover(function() {
            if ($(window).width() >= 1200)
                $(this).children('ul').stop().show();
        }, function() {
            if ($(window).width() >= 1200)
                $(this).children('ul').stop().hide();
        });
        
        $('.details').width($(window).width());
        $(window).resize(function(event) {
            $('.details').width($(window).width()); 
        });

        /* 菜单按钮 */
        $('#menu-toggle').click(function(event) {
            $('#menu').slideToggle( 'fast' );
            $(this).toggleClass('open');
        });

        
        // pc端设置video最小尺寸
        var isMobile = /mobile|android/ig.test( navigator.userAgent );
        if ( !isMobile ) {
            $('video').not('.preload video').css({
                minHeight: '700px',
                minWidth: '700px'
            });
        }
        
    });

})(jQuery);
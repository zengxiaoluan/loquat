jQuery(document).ready(function($) {

    ( function( $ ) {

        var api = wp.customize;

        api( 'footer_bg_color', function( value ) {
            value.bind( function( newval ) {
                $('footer').css( 'background-color', newval );
            } );
        } );

        api( 'cooperation_bg_color', function( value ) {
            value.bind( function( newval ) {
                $('.cooperation').css( 'background-color', newval );
            } );
        } );

        api( 'platform_bg_color', function( value ) {
            value.bind( function( newval ) {
                $('.platform').css( 'background-color', newval );
            } );
        } );

        api( 'qualify_bg_color', function( value ) {
            value.bind( function( newval ) {
                $('.qualify').css( 'background-color', newval );
            } );
        } );
        
    } )( jQuery );

    
});


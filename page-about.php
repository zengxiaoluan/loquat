<?php
/*
Template Name: about
*/
?>

<?php 
    get_header();
?>
   
    <div class="about">
        <img class="banner" src="<?php echo get_template_directory_uri(); ?>/images/banner.png" alt="">
        <div class="container text-center">
            <dl>
                <dt class="h2 class-baseline"><?php _e( '公司简介', 'loquat' ); ?></dt>
                <dd>
                    <p><?php _e( '湖南文沥征信数据服务有限公司（简称文沥），于2014年在长沙注册正式成立。', 'loquat' ); ?></p>

                    <p><?php _e( '文沥作为数据时代领先的金融科技服务商，采集、分析、整合企业数据资源，自动实时的反应企业运营的真实状况，为银行、基金等金融机构提供高效精准的企业经营业务数据；拓展金融机构的服务范围，加快金融机构的审核效率，为其提供动态的新型大数据贷后风控管理服务；并帮助企业得到迅速便捷低成本的融资服务。', 'loquat' ); ?></p>

                    <p><?php _e( '文沥本着“数据为本，科技铸金”的金融科技理念，在大数据征信领域刻苦深耕，与中南大学成立了“金融数据研究所”和“中南大学产学研基地”，团队由金融、信息、计算机专业方面的专家组成，在量化建模、最优化、数据挖掘与大数据技术方面有丰富的研究经验。', 'loquat' ); ?></p>

                    <p><?php _e( '文沥在平台系统及数据采集方面构筑了雄厚的基础，以全球先进的数据集成技术为核心，结合B2B电子商务以及企业实时经营性数据应用整合的经验，成为国内专业提供价值链整合集成软件、云平台、金融服务和企业征信服务的高科技公司。', 'loquat' ); ?></p>

                    <p><?php _e( '文沥将上述前沿大数据技术成功运用于国内淘宝天猫商户和工程业界，通过分析天猫商户订单的实时数据和历史数据、建筑企业经营数据建立数据模型，为金融机构提供贷前审核、贷中管理和贷后监控等征信数据整套服务。', 'loquat' ); ?></p>
                </dd>
            </dl>
            <dl>
                <dt class="h2 class-baseline"><?php _e( '合作伙伴', 'loquat' ); ?></dt>
                <dd>
                    <div class="row">
                        <div class="col-md-2 col-xs-6"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/partners/平安银行.png" alt="<?php esc_attr_e( '平安银行', 'loquat' ); ?>"></div>
                        <div class="col-md-2 col-xs-6"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/partners/华夏银行.png" alt="<?php esc_attr_e( '华夏银行', 'loquat' ); ?>"></div>
                        <div class="col-md-2 col-xs-6"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/partners/中南大学.png" alt="<?php esc_attr_e( '中南大学', 'loquat' ); ?>"></div>
                        <div class="col-md-2 col-xs-6"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/partners/长沙银行.png" alt="<?php esc_attr_e( '长沙银行', 'loquat' ); ?>"></div>
                        <div class="col-md-2 col-xs-6"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/partners/世旗资本.png" alt="<?php esc_attr_e( '世旗资本', 'loquat' ); ?>"></div>
                        <div class="col-md-2 col-xs-6"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/partners/宇纳资本.png" alt="<?php esc_attr_e( '宇纳资本', 'loquat' ); ?>"></div>
                    </div>
                </dd>
            </dl>
        </div>
    </div>

    <style>
        .about{
            font-family: 'Microsoft Yahei';
        }
        .about dd{
            margin-bottom: 2rem;
        }
        .about .class-baseline{
            position: relative;
            margin-bottom: 3rem;
        }
        .about .class-baseline:before{
            content: '';
            position: absolute;
            left: 50%;
            top: 3.5rem;
            transform: translate(-50%);
            height: 3px;
            width: 3rem;
            background: #35acff;
        }
        img.banner{
            width: 100%;
            height: auto;
            padding: 0;
            margin-bottom: 3rem;
        }
    </style>
    
<?php
    get_footer();
?>
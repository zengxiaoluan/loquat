    
    <div class="col-md-4 sidebar hidden-print">
        <ul id="sidebar" class="list-unstyled">
            <?php dynamic_sidebar( 'orange' ); ?>
        </ul>
    </div>

    <style>
        .sidebar{
            transition: all .5s;
        }
        .sidebar .widgettitle{
            background-color: #5bc0de;
            color: white;
            padding: 1rem;
        }
        .sidebar:hover{
            box-shadow: 0px 1px 15px #d2d0d0,0px -1px 15px #d2d0d0;
        }
        .sidebar ul#sidebar > li{
            border-bottom: 1px dotted #eee;
            list-style: none;
            padding-bottom: 1em;
        }
        .sidebar table{
            width: 100%;
            text-align: center;
        }
        .sidebar .tagcloud a{
            border: 1px solid #eee;
            display: inline-block;
            margin: 0.5rem;
            padding: 0.5rem;
            border-radius: 5px;
        }
    </style>

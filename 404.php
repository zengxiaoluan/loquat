<?php 
    get_header();
?>

    <div class="container">
        <h1 class="text-center text-info"> <?php _e( 'WOW!这是一片大数据都没有记录的地方···', 'orange' ); ?> </h1>
        <img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/404.gif" alt="404 page">
    </div>

<?php 
    get_footer();
?>
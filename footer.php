    
    <footer class="hidden-print" id="footer">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12 address">
                    <h3><?php _e( '公司地址', 'loquat' ); ?></h3>
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span class="h5"><?php _e( '湖南省长沙市岳麓区尖山路39号中电软件园9栋801', 'loquat' ); ?></span>
                    <p><?php _e( '邮编：410205', 'loquat' ); ?></p>
                    <div class="map" id="allmap"></div>
                </div>
            </div>
            <div class="row text-center copyright">
                <div class="col-md-4 col-xs-12">
                    <?php _e( '&copy;All Rights Reserved.湖南文沥征信数据服务有限公司', 'loquat' ); ?>
                    <br>
                    <a target="_blank" href="http://www.miibeian.gov.cn/" rel="nofollow"><?php echo get_option('zh_cn_l10n_icp_num');?></a>
                </div>
                <?php 
                    wp_nav_menu( array(
                        'theme_location'        =>  'law',
                        'menu_class'            =>  'law-menu',
                        'fallback_cb'           =>  false,
                        'container_class'       =>  'col-md-4 col-xs-12'
                    ) );
                ?>
                <?php 
                    wp_nav_menu( array(
                            'theme_location'    =>  'social',
                            'menu_class'        =>  'social-menu',
                            'link_before'       =>  '<div class="hidden">',
                            'link_after'        =>  '</div>',
                            'fallback_cb'       =>  false,
                            'container_class'   =>  'col-md-4 col-xs-12'
                        )
                    ); 
                ?>
            </div>
        </div>
        <div id="back-top" class="">
            <i title="<?php _e( '回到顶部', 'loquat' ); ?>" class="fa fa-chevron-up" aria-hidden="true"></i>
        </div>
    </footer>

    <style>
        footer{
            color: #fff;
            background: url(<?php echo get_template_directory_uri() ?>/images/bg.png) no-repeat center center;
            background-size: cover;
            padding-bottom: 1rem;
        }
        .copyright{
            margin-top: 2rem;
        }
        footer a{
            text-decoration: none;
            color: #fff;
        }
        .address{
            height: 100%;
        }
        .map{
            height: 300px;
        }
        .law-menu,.social-menu{
            display: inline-block;
            margin-left: 0;
            padding-left: 0;
        }
        ul.law-menu li, ul.social-menu li{
            list-style: none;
            float: left;
            margin: 0em 1em;
        }
        ul.social-menu li{
            position: relative;
        }
        .social-menu .weixin-img{
            height: auto;
            position: absolute;
            bottom: 2em;
            left: 50%;
            transform: translateX(-50%);
            width: 100px;
        }
        .social-menu .weixin-img img{
            width: 100px;
        }
        .social-menu a{
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
        }
        .social-menu a[href*="weibo.com"]:before{
            content:"\f18a";
        }
        .social-menu a[href*="baidu.com"]:before{
            content:"\f14b";
        }
        .social-menu a[href*="weixin.com"]:before{
            content:"\f1d7";
        }
        .social-menu a[href*="tel:"]:before{
            content:"\f095";
        }
        .social-menu a[href*="mailto:"]:before{
            content:"\f0e0";
        }
        #back-top{
            background-color: #333;
            border-radius: 5px;
            position: fixed;
            padding: 1em;
            right: 5%;
            bottom: 100px;
            transition: all 0.3s;
            cursor: pointer;
            display: none;
        }
        #back-top:hover{
            box-shadow: 0px 5px 3px #333,5px 0px 3px #333,0px -5px 3px #333,-5px 0px 3px #333;
        }
    </style>

    <script>
        ;(function ($) {
            var image = new Image();
                image.src = "<?php echo get_template_directory_uri(); ?>/images/公司公众号.jpg";
                image.onload = function () {
                    $('a[href*="weixin.com"]').append("<div class='hidden weixin-img'><img src='"+ image.src +"'></div>");
                };

            $('a[href*="weixin.com"]').click(function(event) {
                event.preventDefault();
            }).hover(function() {
                $('.weixin-img').removeClass('hidden').addClass('show');
            }, function() {
                $('.weixin-img').removeClass('show').addClass('hidden');
            });
        })(jQuery);
    </script>

    <script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=FdSzVtUSuxDUvW3wzSmIutvuppftCRQ2&s=1"></script>
    <script>
        var map = new BMap.Map("allmap");
        map.centerAndZoom(new BMap.Point(112.887561,28.233702),11);
        map.enableScrollWheelZoom(true);

        var new_point = new BMap.Point(112.887561,28.233702);
        var marker = new BMap.Marker(new_point);  
        map.addOverlay(marker);              
        map.panTo(new_point); 
    </script>
    <?php wp_footer(); ?>
</body>
</html>
<?php
/*
Template Name: retailer
*/
?>

<?php 
    get_header();
?>
    
    <div class="retailer">
        <div class="container">
            <p class="product-title text-center"><?php _e( '电商流水贷', 'loquat' ); ?></p>
            <?php if ( wp_is_mobile() ):?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/3.png" alt="" class="img-responsive">
            <?php else: ?>
                <video autoplay loop class="center-block" poster="<?php echo get_template_directory_uri(); ?>/images/3.png">
                    <source src="<?php echo get_template_directory_uri(); ?>/video/3.mp4">
                </video>
            <?php endif; ?>
            <p class="product-desc text-center"><?php _e( '面向全国电商企业，提供额度高、利率优的金融服务', 'loquat' ); ?></p>
            <div class="features row text-center">
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '额度高', 'loquat' ); ?></p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '利率低', 'loquat' ); ?></p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '纯信用', 'loquat' ); ?></p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="retailer-feature"><?php _e( '审批快', 'loquat' ); ?></p>
                </div>
            </div>
            <div class="retailer-bank text-center">
                <p><a href="https://cscb.welinkcredit.com/">&#60;&#60;<?php _e( '长沙银行', 'loquat' ); ?></a></p>
                <p><a href="https://dsd.welinkcredit.com/">&#60;&#60;<?php _e( '平安银行', 'loquat' ); ?></a></p>
            </div>
        </div>
    </div>

<?php 
    get_footer( 'none' );
?>

<style>
    .features{
        margin-bottom: 2rem;
    }
    .product-desc{
        margin-bottom: 4rem;
    }
    .retailer-feature{
        color: #35acff;
        padding: 1rem 2rem;
        border: 1px solid #35acff;
        border-radius: 3px;
        margin: 0 4rem 1rem 4rem;
    }
    .retailer{
        padding-bottom: 8rem;
    }
</style>
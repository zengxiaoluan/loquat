<?php 
    get_header();
?>
    <div class="container-fluid">
        <div class="posts">
            <?php 
                if ( have_posts() ) : 

                    while ( have_posts() ) : the_post();
                        get_template_part( 'sections/part', 'news' );
                    endwhile;
                    
                endif;
            ?>
        </div>
        <div class="container text-center">
            <?php 
                the_posts_pagination( array(
                    'mid_size'           => 5,
                    'screen_reader_text' => false,
                    'prev_text'          => __( '上一页', 'orange' ),
                    'next_text'          => __( '下一页', 'orange' ),
                    'before_page_number' => '<span class="meta-nav">' . __( '页', 'orange' ) . ' </span>',
                ) );
            ?>
        </div>
    </div>
    <!-- imagesloaded -->
    <script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
    <script>
        jQuery.fn.extend({
            waterLayout: function (water_el) {
                var $ = $ || jQuery || window.jQuery;
                /*需要进行布局的元素*/
                var $posts      = $( water_el );

                /*每列宽度*/
                var box_width   = $posts.eq(0).outerWidth();

                /*每列的高度*/
                var col_heights = [];

                /*列数*/
                var col_num = Math.floor( $(window).width() / box_width );

                /*设置posts居中和宽度*/
                this.width( col_num * box_width ).css('margin', '1em auto');

                $posts.each(function(index, $post) {

                    var current_post_outerHeigt = $(this).outerHeight() + 10

                    if ( index < col_num )  {
                        col_heights[index] = current_post_outerHeigt;

                        $(this).css({
                            position: 'absolute',
                            top: '0px',
                            left: index * box_width + 'px',
                        });
                    }else{
                        var min_height = Math.min.apply(null, col_heights);
                        var min_index = $.inArray(min_height, col_heights);
                        $(this).css({
                            position: 'absolute',
                            top: min_height + 'px',
                            left: min_index * box_width + 'px',
                        });
                        col_heights[min_index] += current_post_outerHeigt
                    }

                });
                this.height( Math.max.apply( null, col_heights ) );
                return this;
            }
        })
    </script>
    <script>
        jQuery(document).ready(function($) {
            $('.posts').imagesLoaded(function () {
                $('.posts').waterLayout('.post');
            })
        });
    </script>

    <style>
        .container-fluid{
            background: rgb(244,245,247);
        }
        .posts{
            position: relative;
        }
        <?php
            if( wp_is_mobile() ){
                // 移动端宽度自适应
                echo ".posts .post{width: 100%;}";
            }else{
                echo ".posts .post{width: 420px;}";
            }
        ?>
        .posts .magic{
            border: 1px solid rgb(238,238,238);
            margin: 0.5em;
            padding: 0.5em;
            background: white;
            transition: all 0.5s;
        }
        .post .meta{
            margin: 1em;
        }
        .magic:hover{
            box-shadow: 0px 1px 15px #d2d0d0,0px -1px 15px #d2d0d0;
            transform: translateY(-0.5em);
        }
    </style>

<?php 
    get_footer();
?>
<?php get_header(); ?>

    <div class="container-fluid" class="author">

        <?php 
            $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); 
        ?>

        <div class="profile text-center">
            <div class="avatar-img">
                <?php echo get_avatar( get_the_author_meta( 'user_email' ), 100, '', true ); ?>
            </div>
            <h6><?php echo $curauth->nickname; ?></h6>
            <dl>
                <dd><a target="_blank" href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
                <dd><?php echo $curauth->user_description; ?></dd>
            </dl>
        </div>

        <ul class="posts-list text-center">
            <h2><?php _e( '他的文章：', 'orange' ); ?> </h2>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <li>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
                    <?php the_title(); ?></a>,
                    <?php the_time('d M Y'); ?> in <?php the_category('&');?>
                </li>

            <?php endwhile; else: ?>
                <p><?php _e('这个作者很勤奋，一篇文章也没有写！', 'orange'); ?></p>
            <?php endif; ?>
        </ul>
    
        <div class="text-center">
            <?php
                the_posts_pagination( array(
                    'mid_size'           => 5,
                    'prev_text'          => __( '上一页', 'orange' ),
                    'next_text'          => __( '下一页', 'orange' ),
                    'before_page_number' => '<span class="meta-nav">' . __( '页', 'orange' ) . ' </span>',
                ) );
            ?>
        </div>

        <ul class="posts-list text-center">
            <h2 class="text-center"><?php _e( '其它作者：', 'orange' ); ?></h2>
            <?php wp_list_authors(); ?>
        </ul>

    </div>

    <style>
        .author img.avatar{
            width: 100px;
            height: 100px;
            border-radius: 50%;
            transition: all .2s;
        }
        .author img.avatar:hover{
            border: 5px solid #eee;
        }
        .author .avatar-img{
            height: 100px;
            margin: 0 auto;
            width: 100px;
        }
        .posts-list li{
            margin: 0 auto;
            max-width: 100%;
            white-space: nowrap;
            word-wrap: normal;
            text-overflow: ellipsis;
            overflow: hidden;
            word-break: break-all; 
        }
    </style>

<?php get_footer(); ?>
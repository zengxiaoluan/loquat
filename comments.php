<?php

    if ( post_password_required() ) {
        return;
    }
?>

    <div id="comments" class="comments-area">

        <?php if ( have_comments() ) : ?>

            <?php the_comments_navigation(); ?>

            <ol class="comment-list">
                <?php
                    wp_list_comments( array(
                        'style'       => 'ul',
                        'short_ping'  => true,
                        'avatar_size' => 42,
                    ) );
                ?>
            </ol><!-- .comment-list -->

            <?php the_comments_navigation(); ?>

        <?php endif; // Check for have_comments(). ?>

        <?php
            // If comments are closed and there are comments, let's leave a little note, shall we?
            if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
        ?>
            <p class="no-comments"><?php _e( '评论已经关闭。', 'orange' ); ?></p>
        <?php endif; ?>

        <?php

            $commenter = wp_get_current_commenter();
            $req = get_option( 'require_name_email' );
            $aria_req = ( $req ? " aria-required='true'" : '' );

            $fields =  array(

                'author' =>
                    '<p class="comment-form-author"><label for="author">' . __( '您的昵称：', 'orange' ) . '</label> ' .
                    ( $req ? '<span class="required">*</span>' : '' ) .
                    '<input id="author" class="form-control" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
                    '" size="30"' . $aria_req . ' /></p>',
                'email' =>
                    '<p class="comment-form-email"><label for="email">' . __( '您的邮箱：', 'orange' ) . '</label> ' .
                    ( $req ? '<span class="required">*</span>' : '' ) .
                    '<input id="email" class="form-control" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                    '" size="30"' . $aria_req . ' /></p>',
                /*'url' =>
                    '<p class="comment-form-url"><label for="url">' . __( 'Website', 'domainreference' ) . '</label>' .
                    '<input id="url" class="form-control" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
                    '" size="30" /></p>',*/
            );

            apply_filters( "comment_form_default_fields",  $fields);

            comment_form( array(
                'title_reply_before' => '<h3 id="reply-title" class="text-muted comment-reply-title">',
                'title_reply_after'  => '</h3>',
                'class_submit'       => 'btn btn-info',
                'class_form'         => 'form-horizontal',
                'fields'             => apply_filters( "comment_form_default_fields",  $fields), 
                'comment_field'      => '<p class="comment-form-comment"><label for="comment">' . __( '您的评论：', 'orange' ) .
                                '</label><textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
                                '</textarea></p>',
            ) );
        ?>

    </div><!-- .comments-area -->

    <style>
        .comments-area{
            border-top: 1px solid #eee;
            margin-top: 3em;
        }
        .comments-area cite{
            font-style: normal;
        }
        .comments-area ol,.comments-area ul{
            list-style: none;
        }
        .comments-area ol{
            margin: 0;
            padding: 0;
        }
        .comments-area .comment-respond{
            padding-bottom: 1em;
        }
        .comments-area .comment-list{
        }
        .comments-area .comment-list > .comment{
            border-bottom: 1px solid #eee;
            padding-bottom: 1em;
            margin-top: 1em;
        }
    </style>

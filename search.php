<?php 
    get_header();
 ?>

   <div id="primary" class="container">
        <main id="main" class="site-main" role="main">

            <?php if ( have_posts() ) : ?>

                <header class="page-header">
                    <h1 class="page-title"><?php printf( __( '%s的搜索结果：', 'orange' ), '<span class="text-danger h1">' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
                </header>

                <?php

                while ( have_posts() ) : the_post();

                    get_template_part( 'sections/content', 'search' );

                endwhile;

                the_posts_pagination( array(
                    'prev_text'          => __( '上一页', 'orange' ),
                    'next_text'          => __( '下一页', 'orange' ),
                    'before_page_number' => '<span class="meta-nav">' . __( '页', 'orange' ) . ' </span>',
                ) );

            else :
                ?>
                <h1><?php _e( '没有找到相关内容，再搜一搜', 'orange' ); ?>?</h1>
            <?php
                endif;
            ?>

        </main>
    </div>

<?php 
    get_footer();
 ?>